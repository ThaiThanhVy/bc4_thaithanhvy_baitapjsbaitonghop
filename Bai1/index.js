function inRa() {
    var myTableDiv = document.getElementById("myDynamicTable");
    var table = document.createElement('TABLE');
    table.border = '1';
    var tableBody = document.createElement('TBODY');
    table.appendChild(tableBody);
    for (var i = 1; i <= 10; i++) {
        var tr = document.createElement('TR');
        tableBody.appendChild(tr);
        for (var j = i; j <= 100; j += 10) {
            var td = document.createElement('TD');
            td.width = '75';
            td.appendChild(document.createTextNode(j));
            tr.appendChild(td);
        }
    }
    myTableDiv.appendChild(table);
}
addTable();