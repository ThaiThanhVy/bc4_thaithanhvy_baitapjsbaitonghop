var numArr = [];

// Thêm số
function themSo() {
    var themSo = document.getElementById("txt-them-so").value * 1;
    // console.log('themSo: ', themSo);
    document.getElementById("txt-them-so").value = "";
    numArr.push(themSo);
    document.getElementById("kq-them-so").innerText = `${numArr}`
}
// end Thêm số

function isprime(n) {

    let flag = 1;

    if (n < 2) return flag = 0;
    let i = 2;
    while (i < n) {
        if (n % i == 0) {
            flag = 0;
            break;
        }
        i++;
    }

    return flag;
}

result = [];
function soNguyen() {
    for (let i = 0; i < numArr.length; i++) {
        if (isprime(numArr[i]) == 1) result.push(numArr[i]);
    }
    document.getElementById("so-nguyen-to").innerHTML =`${result}`
}